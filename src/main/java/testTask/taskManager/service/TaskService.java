package testTask.taskManager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import testTask.taskManager.domain.Task;
import testTask.taskManager.domain.User;
import testTask.taskManager.repository.TaskRepository;
import testTask.taskManager.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class TaskService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskRepository taskRepository;

    public Task save(Task task) {
        return taskRepository.save(task);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findById(String id) {
        return taskRepository.findById(id).get();
    }

    public List<Task> getAvailableTask(User user) {
        List<Task> tasks = new ArrayList<>();
        for (Task task : taskRepository.findAll()) {
            Set<User> taskUsers = task.getUsers();
            if (taskUsers == null || taskUsers.isEmpty()) {
                tasks.add(task);
            } else {
                for (User taskUser : taskUsers) {
                    if (taskUser.getEmail().equals(user.getEmail())) {
                        tasks.add(task);
                    }
                }
            }
        }
        System.out.println(tasks);

        return tasks;
    }

    public Task updateById(String id, Task updatedTask) {
        updatedTask.setId(id);
        taskRepository.save(updatedTask);
        return updatedTask;
    }

    void deleteTaskById(String id) {
        if (taskRepository.existsById(id)) {
            taskRepository.deleteById(id);
        }
    }

}
