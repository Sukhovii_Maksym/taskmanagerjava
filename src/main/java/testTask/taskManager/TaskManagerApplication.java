package testTask.taskManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import testTask.taskManager.domain.Role;
import testTask.taskManager.domain.User;
import testTask.taskManager.repository.RoleRepository;
import testTask.taskManager.repository.UserRepository;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class TaskManagerApplication {
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(TaskManagerApplication.class, args);
    }

    @Bean
    CommandLineRunner initRoles() {

        return args -> {

            Role adminRole = roleRepository.findByRole("ADMIN");
            if (adminRole == null) {
                Role newAdminRole = new Role();
                newAdminRole.setRole("ADMIN");
                roleRepository.save(newAdminRole);
            }

            Role userRole = roleRepository.findByRole("USER");
            if (userRole == null) {
                Role newUserRole = new Role();
                newUserRole.setRole("USER");
                roleRepository.save(newUserRole);
            }
        };

    }

    @Bean
    CommandLineRunner initFirstAdmin() {

        return args -> {

            User admin = userRepository.findByEmail("admin@mail.com");
            if (admin == null) {
                admin = new User();
                admin.setEmail("admin@mail.com");
                admin.setPassword(bCryptPasswordEncoder.encode("admin"));
                admin.setFullname("Admin Adminych");
                Set<Role> roleSet = new HashSet<>();
                roleSet.add(roleRepository.findByRole("ADMIN"));
                admin.setRoles(roleSet);
                userRepository.save(admin);
            }


        };

    }
}
