package testTask.taskManager.domain;

public enum TaskStatus {
    NEW, IN_PROGRESS, REVIEW, DONE
}
