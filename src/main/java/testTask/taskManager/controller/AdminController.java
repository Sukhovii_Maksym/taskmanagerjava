package testTask.taskManager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import testTask.taskManager.domain.Task;
import testTask.taskManager.domain.TaskStatus;
import testTask.taskManager.domain.User;
import testTask.taskManager.repository.TaskRepository;
import testTask.taskManager.repository.UserRepository;

@Controller
public class AdminController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskRepository taskRepository;

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView dashboard() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
        modelAndView.addObject("fullName", "Welcome " + user.getFullname());
        modelAndView.addObject("allUsers", userRepository.findAll());
        modelAndView.addObject("allTasks", taskRepository.findAll());
        modelAndView.addObject("task", new Task());
        modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
        modelAndView.setViewName("dashboard");
        return modelAndView;
    }

    @RequestMapping(value = "/addTask", method = RequestMethod.POST)
    public ModelAndView addTask(@ModelAttribute("task") Task newTask) {
        newTask.setStatus(TaskStatus.NEW);
        taskRepository.save(newTask);
        return new ModelAndView("redirect:/dashboard");
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteTask(@PathVariable("id") String id) {
        taskRepository.deleteById(id);
        return new ModelAndView("redirect:/dashboard");
    }

    @GetMapping("/done/{id}")
    public ModelAndView makeTaskStatusDone(@PathVariable("id") String id) {
        Task task = taskRepository.findById(id).get();
        task.setStatus(TaskStatus.DONE);
        taskRepository.save(task);
        return new ModelAndView("redirect:/dashboard");
    }

    @GetMapping("/edit/{id}")
    public ModelAndView makeTaskStatusDone(@PathVariable("id") String id, Model model) {
        System.out.println(id);
        Task task = taskRepository.findById(id).get();
        model.addAttribute("task", task);
        return new ModelAndView("redirect:edit_task/{id}");
    }

    @PostMapping("/update/{id}")
    public ModelAndView updateTask(@PathVariable("id") String id, @ModelAttribute("task") Task task) {
        System.out.println(task);
        task.setId(id);
        task.setStatus(TaskStatus.NEW);
        taskRepository.save(task);
        return new ModelAndView("redirect:/dashboard");
    }

    @GetMapping("/edit_task/{id}")
    public ModelAndView editTask(@PathVariable("id") String id) {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
        modelAndView.addObject("allUsers", userRepository.findAll());
        modelAndView.addObject("task", taskRepository.findById(id).get());
        modelAndView.setViewName("edit_task");
        return modelAndView;
    }
}
