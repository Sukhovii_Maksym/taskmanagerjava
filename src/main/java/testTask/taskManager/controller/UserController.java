package testTask.taskManager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import testTask.taskManager.domain.Task;
import testTask.taskManager.domain.TaskStatus;
import testTask.taskManager.domain.User;
import testTask.taskManager.repository.UserRepository;
import testTask.taskManager.service.TaskService;

@Controller
public class UserController {
    @Autowired
    TaskService taskService;

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/userpage", method = RequestMethod.GET)
    public ModelAndView userpage() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(auth.getName());
        modelAndView.addObject("currentUser", user);
        modelAndView.addObject("fullName", "Welcome " + user.getFullname());
        modelAndView.addObject("availableTasks", taskService.getAvailableTask(user));
        modelAndView.setViewName("userpage");
        return modelAndView;
    }

    @GetMapping("/take_task/{id}")
    public ModelAndView makeTaskStatusInProgress(@PathVariable("id") String id) {
        Task task = taskService.findById(id);
        task.setStatus(TaskStatus.IN_PROGRESS);
        taskService.save(task);
        return new ModelAndView("redirect:/userpage");
    }

    @GetMapping("/finish_task/{id}")
    public ModelAndView makeTaskStatusReview(@PathVariable("id") String id) {
        Task task = taskService.findById(id);
        task.setStatus(TaskStatus.REVIEW);
        taskService.save(task);
        return new ModelAndView("redirect:/userpage");
    }
}
