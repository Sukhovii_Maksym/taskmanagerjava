package testTask.taskManager.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import testTask.taskManager.domain.Task;

import java.util.Optional;

public interface TaskRepository extends MongoRepository<Task, String> {
    Optional<Task> findById(String id);
}
