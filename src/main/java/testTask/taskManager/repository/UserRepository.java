package testTask.taskManager.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import testTask.taskManager.domain.User;

public interface UserRepository extends MongoRepository<User, String> {
    User findByEmail(String email);
}
