package testTask.taskManager.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import testTask.taskManager.domain.Role;

public interface RoleRepository extends MongoRepository<Role, String> {
    Role findByRole(String role);
}
